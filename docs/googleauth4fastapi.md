# FastAPI에서 GoogleAuth 사용 <sup>[1](#footnote_1)</sup>

## 개요
FastAPI를 Google 인증과 통합하려면 Google의 OAuth 2.0 프로토콜을 사용하여야 하며, 사용자가 Google 자격 증명을 사용하여 FastAPI 어플리케이션에 로그인할 수 있도록 해야 한다. 다음은 이 통합을 위한 일반적인 단계이다.

### Google 클라우드 콘솔에서 프로젝트 생성

- 아직 프로젝트를 만들지 않았다면 Google 클라우드 콘솔(https://console.cloud.google.com/)로 이동하여 새 프로젝트를 만든다.
- "`APIs & Services > Dashboard`" 섹션에서 "`Google+ API`"을 사용할 수 있도록 설정한다.

### OAuth 2.0 자격 증명 생성
- Google Cloud 콘솔에서 “`APIs & Services > Credentials`”으로 이동한다.
- "`Create Credentials`"를 클릭하고 "`OAuth client ID`"를 선택한다.
- 어플리케이션 타입으로 "`Web application`"을 선택한다.
- "`Authorized redirect URIs`"에 FastAPI 어플리케이션의 URL(예: http://localhost:8000)을 추가한다.

### 필수 라이브러리 설치
pip으로 필요한 라이브러리를 설치한다.

```python
pip install fastapi
pip install uvicorn
pip install python-jose[cryptography]
pip install python-multipart
pip install requests
```

### FastAPI에서 Google 인증 구현
다음은 Google 인증을 FastAPI와 통합하는 방법에 대한 간단한 예이다.

```python
from fastapi import FastAPI, Depends
from fastapi.security import OAuth2PasswordBearer
import requests
from jose import jwt

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# Replace these with your own values from the Google Developer Console
GOOGLE_CLIENT_ID = "your-google-client-id"
GOOGLE_CLIENT_SECRET = "your-google-client-secret"
GOOGLE_REDIRECT_URI = "your-google-redirect-uri"

@app.get("/login/google")
async def login_google():
    return {
        "url": f"https://accounts.google.com/o/oauth2/auth?response_type=code&client_id={GOOGLE_CLIENT_ID}&redirect_uri={GOOGLE_REDIRECT_URI}&scope=openid%20profile%20email&access_type=offline"
    }

@app.get("/auth/google")
async def auth_google(code: str):
    token_url = "https://accounts.google.com/o/oauth2/token"
    data = {
        "code": code,
        "client_id": GOOGLE_CLIENT_ID,
        "client_secret": GOOGLE_CLIENT_SECRET,
        "redirect_uri": GOOGLE_REDIRECT_URI,
        "grant_type": "authorization_code",
    }
    response = requests.post(token_url, data=data)
    access_token = response.json().get("access_token")
    user_info = requests.get("https://www.googleapis.com/oauth2/v1/userinfo", headers={"Authorization": f"Bearer {access_token}"})
    return user_info.json()

@app.get("/token")
async def get_token(token: str = Depends(oauth2_scheme)):
    return jwt.decode(token, GOOGLE_CLIENT_SECRET, algorithms=["HS256"])

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
```

1. 이 예에서는 두 개의 엔드포인트를 제공한다. Google 로그인 플로우를 시작하기 위한 `/login/google`과 액세스 토큰을 사용하여 인증 코드를 교환하기 위한 `/auth/google`이다. 사용자 정보는 Google의 userinfo 엔드포인트에서 검색할 수 있다. 또한 인증 성공 후 얻은 JWT 토큰을 디코딩하고 확인하기 위한 엔드포인트 `/token`이 포함되어 있다.

`your-google-client-id`, `your-google-client-secret` 및 `your-google-redirect-uri`을 Google Cloud 콘솔 프로젝트의 적절한 값으로 바꿔야 한다.

1. FastAPI 어플리케이션을 실행한다. 코드를 파일(예: `main.py`)에 저장하고 Uvicorn을 사용하여 FastAPI 어플리케이션을 실행한다.

```bash
$ uvicorn main:app --reload
```

이 단계를 수행하면 FastAPI 어플리케이션이 Google 인증과 통합되어 사용자가 Google 계정을 사용하여 로그인할 수 있다. 이는 간단한 예시이며, 실제 시나리오에서는 오류 사례를 처리하고, 사용자 정보를 안전하게 저장하며, 사용자 관리를 위한 데이터베이스 통합을 추가해야 한다.


<a name="footnote_1">1</a>: 이 페이지는 [Enabling GoogleAuth for Fast API](https://medium.com/@vivekpemawat/enabling-googleauth-for-fast-api-1c39415075ea)을 편역한 것임.
